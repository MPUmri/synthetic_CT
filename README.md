Synthetic CT generation from 3D gradient-echo MRI   
Copyright Véronique Fortier 2018 - MIT License 

When using this code, please cite: 
Fortier, V., Fortin, M. A., Pater, P., Souhami, L., & Levesque, I. R. (2021). A role for magnetic susceptibility in synthetic computed tomography. Physica Medica, 85, 137-146.

-----------------------------------------------------------------------


Run ‘synthetic_CT_main.m’ to generate a synthetic CT

Run ‘validate_sCT_main.m’ to perform a quantitative comparison between the sCT and the corresponding x-ray CT

------------------------------------------------------------------------
** NOTES **

1. Fat water separation must be performed. The 3-point Dixon technique from the ISMRM fat-water separation toolbox used in the publication is   available here: https://www.ismrm.org/workshops/FatWater12/data.htm

    For the 3-point Dixon fat/water separation (note from the ISMRM toolbox):
    A subroutine of the algorithm is implemented in c++ for efficiency. The c++ file (RG.cpp) must be compiled before MATLAB can use it. This can be done in MATLAB by navigating to the source code folder, then typing in the MATLAB command window:

    mex RG.cpp (For Matlab versions after 2020, uses: mex -compatibleArrayDims RG.cpp)
    
    If this doesn't work, you may have to specify a compiler by typing: mex -setup

- - - - - - - - - - - - - - -

2. Phase unwrapping must be perforned. The quality-guided unwrapping technique used in the publication is available at: https://gitlab.com/veronique_fortier/Quality_guided_unwrapping. The resulting 3D matrix should be named 'wfreqUW'.

- - - - - - - - - - - - - - -

3. Background field removal must be performed before performed. A three level background removal technique based on the LBV algorithm is recommended. The recommended function for LBV background removal is available as part of the MEDI toolbox: http://pre.weill.cornell.edu/mri/pages/qsm.html

    A subroutine of the algorithm is implemented in c++ for efficiency. The c++ file (mexMGv6.cpp) must be compiled before MATLAB can use it. This can be done in MATLAB by navigating to the source code folder, then typing in the MATLAB command window:

    mex mexMGv6.cpp (For Matlab versions after 2020, uses: mex -compatibleArrayDims mexMGv6.cpp)
    
    If this doesn't work, you may have to specify a compiler by typing: mex -setup

- - - - - - - - - - - - - - -

4. A QSM map is required for the sCT generation algorithm. The modified IPR-QSM technique used in this publication is available at: https://gitlab.com/MPUmri/IPR_QSM. The resulting 3D matrix should be named 'finalQSM'.






