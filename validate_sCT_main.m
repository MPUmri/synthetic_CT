%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison between the synthetic CT (sCT_segmented) and the x-ray CT
% Veronique Fortier                                                       %
% 2021  
% Please cite: doi.org/10.1016/j.ejmp.2021.05.001                         %
%                                                                         %
% This script is a complete example to generate a synthetic CT according  %
% to the algorithm presented in our 2021 article.                         %                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filesPath_toAdd = strcat(pwd, '/Functions');
addpath(genpath(filesPath_toAdd));


%% Required parameters

% Define path for CT and MRI data
folder_CT = ('/Users/Desktop/Patient1_CT/CT_1.dcm');       
folder_MRI = '/Users/Desktop/Patient1_MR/odd';  
readCT = 1

%% CT dicom reading: read in the data from the CT scan DICOM file
%% this generates a MAT file containing the CT data, which can be re-used
if readCT==1
    folder_CT = '/Users/Desktop/Patient1_CT';
    filename_CT = 'filename_CT';
    ct_dicomRead(folder_CT, filename_CT)
end

%% Load CT mat file (if already generated using the code above)
load('filename_CT')
ct_loaded = double(ct_scan(:,:,:))-1024;
figure(); montage(reshape4montage(permute(ct_loaded, [1,2,3])), 'DisplayRange',[-300 300])
matrixSize_CT = size(ct_loaded);


%% CT spatial localisation information
[X_CT, Y_CT, Z_CT, CT_voxelSize, CT_matrixSize] = getXYZ_CT(folder_CT, ct_scan); 


%% MR spatial localisation information
% this part is hard-coded assuming specific orientations for the MRI and CT (MRI=sagittal, CT=axial)
matrixSize_reshape = [matrixSize(2), matrixSize(3), matrixSize(1)]; %reshaping the data to reference frame of the CT referencial (MRI=sagittal, CT=axial)
voxelSize_reshape = [voxelSize(2), voxelSize(3), voxelSize(1)]; %reshaping the data to reference frame of the CT referencial (MRI=sagittal, CT=axial)
[X,Y,Z] = getXYZ(folder_MRI, matrixSize_reshape, voxelSize_reshape); 


%% Parameters for image registration 

%Reshape the sCT to the same orientation as the CT
% this is hardcoded given the image orientation of the MRI and CT acquisitions 
img = permute(sCT_segmented, [2,3,1]);
img = flip(img, 3);

% Display synthetic CT in the axial orientation
figure(); montage(reshape4montage(img), 'DisplayRange',[-300 300]);

% Display CT in the axial orientation
figure(); montage(reshape4montage(ct_loaded), 'DisplayRange',[-300 300]);

% Define variables for image registration
% the moving image is the synthetic CT
moving = img; %sCT
% the fixed image is the CT
fixed = ct_loaded; %CT

% obtain reference to world coordinates
Rmoving = imref3d(size(moving), voxelSize(3), voxelSize(2), voxelSize(1));
Rfixed = imref3d(size(fixed), CT_voxelSize(1), CT_voxelSize(2), CT_voxelSize(3));

% Define the spatial referencing information for the sCT and CT (required for the registration)
Rmoving.XWorldLimits = [min(X(:)), max(X(:))];
Rmoving.YWorldLimits = [min(Y(:)), max(Y(:))];
Rmoving.ZWorldLimits = [min(Z(:)), max(Z(:))];

Rfixed.XWorldLimits = [min(X_CT(:)), max(X_CT(:))];
Rfixed.YWorldLimits = [min(Y_CT(:)), max(Y_CT(:))];
Rfixed.ZWorldLimits = [min(Z_CT(:)), max(Z_CT(:))];

% configure the registration
regModality = 'multimodal' % CT to MRI
[optimizer, metric] = imregconfig(regModality);
optimizer.MaximumIterations = 300; 
optimizer.InitialRadius = 0.001;


%% Perform Rigid registration
transformType = 'rigid';
[movingRegistered, Rregistered] = imregister(moving, Rmoving, fixed, Rfixed, transformType, optimizer, metric);

% Display the results
figure(); imshowpair(fixed(:,:,120), movingRegistered(:,:,120)); axis off; set(gcf,'color','w'); set(gca, 'fontsize', 20, 'fontweight', 'bold'); title('Rigid registration')

clearvars X X_CT Y Y_CT Z Z_CT


%% Non linear registration
[D,movingRegOptimize] = imregdemons(movingRegistered, fixed);

% Display the results
figure(); imshowpair(ct_loaded(:,:,120), movingRegOptimize(:,:,120)); axis off; set(gcf,'color','w'); set(gca, 'fontsize', 20, 'fontweight','bold');title('Non-linear registration')


%% Quantitative comparison between the sCT and the CT =
%% uses the function compareCT()
[maskObject_CT2, DSC_bone, DSC_air, MAE] = compareCT(CT_matrixSize, movingRegOptimize, fixed);


% Display one slice of the sCT and CT after registration
moving_reg_mask = movingRegOptimize.*maskObject_CT2;
ct_loaded_mask = ct_loaded.*maskObject_CT2;
sliceNum = 230;
figure(); imagesc(squeeze(moving_reg_mask(:,sliceNum,:)) - 1000*ones(matrixSize_CT(1), matrixSize_CT(3)).*(1 - squeeze(maskObject_CT2(:,sliceNum,:)))); colormap gray; colorbar; caxis([-300 300]); axis off; set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');
figure(); imagesc(squeeze(ct_loaded_mask(:,sliceNum,:)) - 1000*ones(matrixSize_CT(1), matrixSize_CT(3)).*(1 - squeeze(maskObject_CT2(:,sliceNum,:)))); colormap gray; colorbar; caxis([-300 300]); axis off; set(gcf,'color','w'); set(gca,'fontsize',20','fontweight','bold');

