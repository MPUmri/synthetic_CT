%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Synthetic CT generation from 3D gradient echo sequence                  %
% Veronique Fortier                                                       %
% 2018                                                                    %
% Please cite: doi.org/10.1016/j.ejmp.2021.05.001                         %
%                                                                         %
% This script is a complete example to generate a synthetic CT according  %
% to the algorithm presented in our 2021 article.                         %
% A complete dataset includes the complex data from a multi-echo GRE      %
% acquistion with 6 echoes.                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filesPath_toAdd = strcat(pwd, '/Functions');
addpath(genpath(filesPath_toAdd));


%% Required parameters

% Define path for MRI data     
folder_MRI = '/Users/Desktop/Patient1_MR/odd';  

gamma = 267.513*10^6;     %rad/sT, for hydrogen

% this threshold sets a minimum value (in ppm) on the thresholding process for air regions
minAirThreshold = 1.75;
% this threshold (in ppm) sets a minimum value for the presence of air
airSusceptibilityThreshold = 1;


%% MRI dicom reading
% Read in imaging data from DICOM files
%
% Required data and information:
% Magnitude data, complex data
% TE, delta TE, voxel size, matrix size,
% central frequency, main field magnitude, and B0 direction 

% % Example for data acquired with a 3T Philips Ingenia
% % using two separate sequences (odd and even TEs):
% % this uses the function complexMultiEchoesDataReading() which is available in Functions/
% % The  data used for the original synthetic CT projected published in 2021 was acquired
% % with two sequences due to limitations on the echo spacing

% % read in the data for odd echoes
% folder_MRI = '/Users/Desktop/Patient1_MR/odd';
% filename_MRI = 'filename_MRI_odd.mat';
% sliceOrientation = 'sagittal'; % sagittal, coronal or axial
% complexMultiEchoesDataReading(folder_MRI, filename_MRI, sliceOrientation)

% % read in the data for even echoes
% folder_MRI = '/Users/Desktop/Patient1_MR/even';
% filename_MRI = 'filename_MRI_even.mat';
% sliceOrientation = 'sagittal'; % sagittal, coronal or axial
% complexMultiEchoesDataReading(folder_MRI, filename_MRI, sliceOrientation)


%% Load data
% Use this section of the code to load mat files created
% with the function complexMultiEchoesDataReading()
% Load odd echoes data first
% odd echoes are used for modified IPR-QSM processing
data=load('filename_MRI_odd'); 

% data classification
MagnData = double(data.dataScan.magnitudeReconstructed);
realIm = double(data.dataScan.real);
imIm = double(data.dataScan.im);

% collect the echo times
TE_all = data.dataScan.TE;

% compute the echo spacing
if length(TE_all) > 1
    delta_TE = TE_all(2) - TE_all(1);
else
    delta_TE = TE_all;
end

% reconstitute the complex data
iField = realIm + 1i*imIm;

% Gather additional parameters
B0_dir = data.dataScan.B0_dir';
voxelSize = data.dataScan.voxelSize;
matrixSize = data.dataScan.matrixSize;
CF = data.dataScan.CF;
mainField = 2*pi*CF / gamma; 

% even echoes
data = load('filename_MRI_even'); 

% data classification
MagnData2 = double(data.dataScan.magnitudeReconstructed);
realIm2 = double(data.dataScan.real);
imIm2 = double(data.dataScan.im);

TE_all2 = data.dataScan.TE;
iField2 = realIm2 + 1i*imIm2;

% clear memory
clearvars data realIm2 imIm2 realIm imIm

index_odd = 0;
% Combine data from odd and even echoes together
for index = 1:length(TE_all)
    iField_flyback(:,:,:,index+index_odd) = iField(:,:,:,index);
    iField_flyback(:,:,:,index+index_odd+1) = iField2(:,:,:,index);    
    magn_flyback(:,:,:,index+index_odd) = MagnData(:,:,:,index);
    magn_flyback(:,:,:,index+index_odd+1) = MagnData2(:,:,:,index);  
    TE_flyback(index+index_odd) = TE_all(index);
    TE_flyback(index+index_odd+1) = TE_all2(index);
    index_odd = index_odd + 1;
end

% clear memory
clearvars iField MagnData iField2 MagnData2    

data_flyback = struct('TE', TE_flyback, 'iField', iField_flyback);


%% Generate sCT algorithm inputs
% Required: QSM (finalQSM), fat image (wfat), water image (wwater), quantitative fat map (f1) and
% water map(w1), an air+bone mask (maskAirBone), a contour mask
% (maskObject), a soft tissue mask (maskSoftTissue)

% Example function for masking 
[maskSoftTissue, maskObject, maskAirBone]=generate_masks(magn_flyback(:,:,:,2), matrixSize);  % Use the echo closest to the first in phase time for fat and water
maskAirBone = logical(maskAirBone);
maskSoftTissue = logical(maskSoftTissue); 
maskObject = logical(maskObject);

% Visualize the masking results
figure(); montage(reshape4montage(maskAirBone), 'DisplayRange',[0 1])
figure(); montage(reshape4montage(maskObject), 'DisplayRange',[0 1])     
figure(); montage(reshape4montage(maskSoftTissue), 'DisplayRange',[0 1])


% Fat water separation must then be performed. The 3-point Dixon technique from the ISMRM
% fat-water separation toolbox used in the publication
% is available here: https://www.ismrm.org/workshops/FatWater12/data.htm
% The resulting matrices for the fat and water complex signals should be respectively named
% 'wfat' and 'wwater'
%
% Example how to call the 3-point Dixon from the ISMRM fat-water separation toolbox
% algoParams.species(1).name = 'water';
% algoParams.species(1).frequency = 4.70;

% % Use a 9 peaks fat spectrum
% algoParams.species(2).frequency = [0.90, 1.30, 1.60, 2.02, 2.24, 2.75, 4.20, 5.19, 5.29];
% algoParams.species(2).relAmps = [88 642 58 62 58 6 39 10 37];
% 
%  % Threshold on magnitude weight for seed points
% algoParams.c1 = 0.75; 
% algoParams.c2 = 0.25; 
% 
% imDataParams.voxelSize=voxelSize;
% imDataParams.FieldStrength=mainField;
% imDataParams.PrecessionIsClockwise=1;
% delta_TE=TE_flyback(2)-TE_flyback(1);
% 
% imDataParams.images=reshape((iField(:,:,:,1:3)),[matrixSize(1),matrixSize(2),matrixSize(3),1,3]); 
% imDataParams.TE=TE_flyback(1:3);
% 
% outParams=fw_i3cm0i_3point_berglund( imDataParams, algoParams); 
% wfreq=outParams.fieldmap; 
% wfat=outParams.species(2).amps;
% wwater=outParams.species(1).amps;
% 
% wfreq=(-wfreq*delta_TE*2*pi); %rescaling


% Normalized the fat and water signals
fatImageNorm = abs(wfat) / max(abs(wfat(:)));   
waterImageNorm = abs(wwater) / max(abs(wwater(:)));     


% Calculate fat and water quantitative maps

waterImageNorm = wwater;
fatImageNorm = wfat;

maskFat = zeros(matrixSize);
maskFat(find(abs(fatImageNorm) > abs(waterImageNorm))) = 1;
maskWater = zeros(matrixSize);
maskWater(find(abs(waterImageNorm) >= abs(fatImageNorm))) = 1;
maskFat = logical(maskFat);
maskWater = logical(maskWater);
maskFat = maskFat.*maskSoftTissue;
maskWater = maskWater.*maskSoftTissue;

[ f1, w1 ] = fatQuantification( wfat, wwater, matrixSize);

% display the results
figure(); montage(reshape4montage(w1), 'DisplayRange', [0 100]); %water fraction


% A B0 field map corrected for the presence of chemical shift is needed for QSM.
% The field map obtained as a result of the fat-water separation can be used for this
% and must be spatially unwrapped.
% The quality-guided unwrapping technique used in the
% publication is available at: https://gitlab.com/veronique_fortier/Quality_guided_unwrapping 
% The resulting 3D matrix should be named 'wfreqUW'.

% A QSM map is required for the sCT generation algorithm. The modified IPR-QSM technique used in this 
% publication is available at: https://gitlab.com/MPUmri/IPR_QSM
% The resulting 3D matrix should be named 'finalQSM'.

clear iField_flyback iField 


%% Synthetic CT generation

% Automatic definition of the threshold to differentiate air and bone on QSM

% Extract the susceptibility in the mask of air and bone regions and normalize it between 0 and 1
airBoneVector = finalQSM(maskAirBone);
airBoneVector2 = (airBoneVector - min(airBoneVector(:)));
airBoneVector3 = airBoneVector2 / (max(airBoneVector2(:)));    % Normalized value

%fcm = fuzzy c-means clustering of the air-bone region
[classes,probabilities] = fcm(airBoneVector3, 2);  

% calculate threshold for air regions
airThreshold = (max(classes)*(max(airBoneVector2(:))) + min(airBoneVector(:)));

 % Determine if air regions are present
if airThreshold<airSusceptibilityThreshold   
    airRegions = 0;
else
    airRegions = 1;
end

% apply minimum air threshold value
% The air threshold should not be too high because of the known underestimation of IPR QSM
airThreshold = min(0.5*airThreshold, minAirThreshold);

% call the CT synthesis function
[sCT_segmented, meanAirSusc, stdAirSusc, meanBoneSusc, stdBoneSusc, maskBone, maskAir] = sCT_generation(finalQSM, matrixSize, maskAirBone, airThreshold, maskObject, f1, fatImageNorm, waterImageNorm, airRegions, w1);

% Visualize the results
figure(); montage(reshape4montage(permute(sCT_segmented,[1,2,3])),'DisplayRange',[-300 300])
