%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison of the sCT with the corresponding CT                         %
%                                                                         %
% Marc-Antoine Fortin, McGill University                                  %
% August 2018                                                             %
%-------------------------------------------------------------------------%
% Inputs: CT matrix size, sCT registered and CT                           %
%-------------------------------------------------------------------------%   
% Outputs: maskHeadContour used for the sCT and the CT quantitative 
% comparison, DSC of bone and air, and mean absolute error (MAE)                                                                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [maskHeadContour_CT2, DSC_bone, DSC_air, MAE_all] = compareCT(CT_matrixSize, movingRegOptimize, fixed)

%% Create a maskHeadContour for the CT based on the sCT
clear maskHeadContour_CT maskHeadContour_CT2
maskHeadContour_CT = zeros(CT_matrixSize);

% remove the background 
maskHeadContour_CT(find(movingRegOptimize >- 300)) = 1;
maskHeadContour_CT(find(movingRegOptimize == 0)) = 0;
maskHeadContour_CT = logical(maskHeadContour_CT);

% create the contour mask
se = strel('diamond',5);
se1 = strel('disk',5);
maskHeadContour_CT2 =  imerode(maskHeadContour_CT,se);
maskHeadContour_CT2 = imdilate(maskHeadContour_CT2, se1);
maskHeadContour_CT2 = maskHeadContour_CT.*maskHeadContour_CT2;

for index=1:CT_matrixSize(3)
    maskHeadContour_CT2(:,:,index) = imfill(maskHeadContour_CT2(:,:,index), 'holes');
end
for index=1:CT_matrixSize(2)
    maskHeadContour_CT2(:,index,:) = imfill(maskHeadContour_CT2(:,index,:), 'holes');
end
for index=1:CT_matrixSize(1)
    maskHeadContour_CT2(index,:,:) = imfill(maskHeadContour_CT2(i,:,:), 'holes');
end

se = strel('diamond',10);
se1 = strel('disk',18);
maskHeadContour_CT2 = imdilate(maskHeadContour_CT2, se1);
for index = 1:CT_matrixSize(3)
    maskHeadContour_CT2(:,:,index) = imfill(maskHeadContour_CT2(:,:,index), 'holes');
end
maskHeadContour_CT2 = imerode(maskHeadContour_CT2, se);

maskHeadContour_CT2(:,:,1:ceil(0.4*CT_matrixSize(3))) = 0;  %Ignore the inferior part of the dataset (neck)
maskHeadContour_CT2(:,:,(end-ceil(0.01*CT_matrixSize(3))):end) = 0; %Ignore the superior part of the dataset (air)

maskHeadContour_CT2 = logical(maskHeadContour_CT2);

% Display the results
% figure(); montage(reshape4montage(permute(maskHeadContour_CT2, [1 2 3])), [0 1]);


%% Calculation of the dice similarity index for air and bone

% Air regions for sCT and CT 
air_sCT = zeros(CT_matrixSize);
air_sCT(find(movingRegOptimize <- 500)) = 1;
air_sCT = air_sCT.*maskHeadContour_CT2;

air_CT = zeros(CT_matrixSize);
air_CT(find(fixed <- 500)) = 1;
air_CT = air_CT.*maskHeadContour_CT2;

% Bone regions for sCT and CT 
bone_sCT = zeros(CT_matrixSize);
bone_sCT(find(movingRegOptimize > 300)) = 1;
bone_sCT = bone_sCT.*maskHeadContour_CT2;

bone_CT = zeros(CT_matrixSize);
bone_CT(find(fixed > 300)) = 1;
bone_CT = bone_CT.*maskHeadContour_CT2;

% DSC bone
bone_common = (bone_sCT.*bone_CT);
DSC_bone = (2*sum(bone_common(:))) / (sum(bone_sCT(:)) + sum(bone_CT(:)))

% DSC air
air_common = (air_sCT.*air_CT);
DSC_air = (2*sum(air_common(:))) / (sum(air_sCT(:))+sum(air_CT(:)))


%% MAE

% Total MAE, consider only the voxels in the head, ignore background
difference = fixed(maskHeadContour_CT2) - movingRegOptimize(maskHeadContour_CT2);
MAE_all = mae(difference) %fixed (CT) - registered (sCT)
MAE_all_std = std(abs(difference))
MAE_all_median = median(abs(difference))
min_all = min((abs(difference)))
max_all = max((abs(difference)))
iqr_all = iqr(abs(difference))

% MAE bone  
maskBoneVoxels = zeros(CT_matrixSize);
maskBoneVoxels(find(fixed > 300)) = 1;
maskBoneVoxels = logical(maskBoneVoxels.*maskHeadContour_CT2);
difference = fixed(maskBoneVoxels) - movingRegOptimize(maskBoneVoxels);
MAE_bone = mae(difference) %fixed (CT) - registered (sCT)
MAE_bone_std = std(abs(difference))
MAE_bone_median = median(abs(difference))
min_bone = min((abs(difference)))
max_bone = max((abs(difference)))
iqr_bone = iqr(abs(difference))

% MAE air
maskAirVoxels = zeros(CT_matrixSize);
maskAirVoxels(find(fixed <- 200)) = 1;
maskAirVoxels = logical(maskAirVoxels.*maskHeadContour_CT2);
difference = fixed(maskAirVoxels) - movingRegOptimize(maskAirVoxels);
MAE_air=mae(difference) %fixed (CT) - registered (sCT)
MAE_air_std=std(abs(difference))
MAE_air_median=median(abs(difference))
min_air=min((abs(difference)))
max_air=max((abs(difference)))
iqr_air=iqr(abs(difference))

% MAE soft tissue
maskSTVoxels=zeros(CT_matrixSize);
maskSTVoxels(find(fixed>-200&fixed<300))=1;
maskSTVoxels=logical(maskSTVoxels.*maskHeadContour_CT2);
test=fixed(maskSTVoxels);
test2=movingRegOptimize(maskSTVoxels);
MAE_ST=mae(test-test2) %fixed (CT) - registered (sCT)
MAE_ST_std=std(abs(test-test2))
MAE_ST_median=median(abs(test-test2))
min_ST=min((abs(test-test2)))
max_ST=max((abs(test-test2)))
iqr_ST=iqr(abs(test-test2))


%% Histogram of the MAE
figure();
diff = (movingRegOptimize(maskHeadContour_CT2) - fixed(maskHeadContour_CT2));
h = histogram(diff);

set(gca,'fontsize', 20, 'fontweight', 'bold');
set(gcf, 'color', 'w');
grid on ;
grid minor;
xlabel('HU');
ylabel('Nb. of voxels');
title('sCT - CT')


%% Sensitivity & Specificity for bones

%true positive
TP = bone_CT.*bone_sCT;

%false negative
FN = (bone_CT-bone_sCT).*bone_CT;

%true negative
TN = ((1 - bone_CT).*maskHeadContour_CT2).*((1 - bone_sCT).*maskHeadContour_CT2);

%false positive
FP = (bone_sCT - bone_CT).*bone_sCT;

sensitivity = sum(TP(:)) / (sum(TP(:)) + sum(FN(:)))
specificity = sum(TN(:)) / (sum(TN(:)) + sum(FP(:)))


%% HU comparison between sCT and CT
Data_sCT = movingRegOptimize(maskHeadContour_CT2);

% CT
figure();
h_CT = histogram(fixed(maskHeadContour_CT2), 'BinWidth', 10);

Bmin_CT = h_CT.BinLimits(1);
BinWidth_CT = h_CT.BinWidth;
NbBins_CT = h_CT.NumBins;
Data_CT = h_CT.Data;

for index = 1:NbBins_CT
    Bmax_CT = Bmin_CT+BinWidth_CT;
    A_CT = find(Data_CT > Bmin_CT & Data_CT < Bmax_CT);
    averageBins_CT(index) = sum(Data_CT(A_CT)) / (h_CT.Values(index));
    averageBins_sCT(index) = mean(Data_sCT(A_CT));
    Bmin_CT = Bmax_CT;    
end

figure(); scatter(averageBins_CT, averageBins_sCT, 'filled', 'linewidth', 2);
hold on
grid on
xlabel('Averages of bins for CT [HU]');
ylabel('Corresponding voxels averages for sCT [HU]');
% title('HU comparison between sCT and CT');
plot([-1000:3000], [-1000:3000], '--k', 'linewidth', 2)
set(gca,'fontsize', 20, 'fontweight', 'bold');
set(gcf,'color','w');

corticalBoneVoxels = find(fixed(maskHeadContour_CT2) > 1500);
corticalBoneVoxels = (length(corticalBoneVoxels) / length(fixed(maskHeadContour_CT2)))*100

NOTcorticalBoneVoxels = find(fixed(maskHeadContour_CT2) < 1500);
NOTcorticalBoneVoxels = (length(NOTcorticalBoneVoxels) / length(fixed(maskHeadContour_CT2)))*100


end
