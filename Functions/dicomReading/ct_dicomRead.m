%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read CT Dicom                                                           %
%                                                                         %
% Véronique Fortier                                                       %
% 2018                                                                    %
% ----------------------------------------------------------------------- %
% Inputs:                                                                 %
% folder is the data folder, filename is the name of the file for the     %
% structure to be saved
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [  ] = ct_dicomRead(folder, filename)

% Read the files in the folder
folder1 = strcat(folder,'/');
filelist = dir(folder1);
folderList = [1];

% Get a list of all the folders
indexFolder = 2;
for indexFile = 1:length(filelist)
    if filelist(indexFile).isdir == 1
        nameFolder = filelist(indexFile).name;
        if nameFolder(1) ~= '.'
            folderList(indexFolder) = filelist(indexFile);
            folderList(indexFolder).name = strcat(folderList(indexFolder).name, '/');
            indexFolder = indexFolder + 1;
        end
    end
end

indexSlice = 1;
% Read data in each folder
for indexFolder = 1:length(folderList)
    if length(folderList)~=1
        folder2 = strcat(folder1, folderList(indexFolder).name);
    else
        folder2 = folder1;
    end

    filelist = dir(folder2);  
    
    indexFile=1;
    while indexFile <= length(filelist)
        if filelist(indexFile).isdir == 1
            filelist = filelist([1:indexFile-1 indexFile+1:end]);   % skip folders
        else
            indexFile = indexFile + 1;
        end
    end


    for indexFile = 1:(length(filelist))
        nameFold = filelist(indexFile).name;
        if nameFold(1) ~= '.'
            info = dicominfo([folder2 filelist(indexFile).name]);
            
            sliceLocation(indexSlice) = info.SliceLocation;

            ct(:,:,indexSlice)  = (dicomread([folder2 filelist(indexFile).name]));  
            indexSlice = indexSlice + 1;
            
            if indexSlice == 2
               slope = info.RescaleSlope;
               intercept = info.RescaleIntercept; 
            end
        end
    end
end

% Reorder the slices (if needed)
slice_reorder=sort(sliceLocation);

for indexSlice = 1:length(slice_reorder)
    currentIndice = find(sliceLocation == slice_reorder(indexSlice));
    ct_scan(:, :, indexSlice) = ct(:, :, currentIndice);
end

% Save 
save (filename, 'ct_scan')

end

