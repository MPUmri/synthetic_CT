%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the spatial position from an MRI DICOM file                 %                                                                     
%                                                                         %
% Marc-Antoine Fortin                                                     %
% 2018                                                                    %
% ----------------------------------------------------------------------- %
% Inputs:                                                                 %
% data folder , matrixSize & voxelSize                                    %
% ----------------------------------------------------------------------- %
% Output:                                                                 %
% Output contains the position (in mm) X, Y & Z in                        %
% the spatial coordinate system of the image                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [X, Y, Z] = getXYZ(folder, matrixSize, voxelSize)

% Read the files in the folder
folder1 = strcat(folder, '/');
filelist = dir(folder1);
folderList = [1];

% Get of list of all the folders
indexFolder=1;
for indexFile = 1:length(filelist)
    if filelist(indexFile).isdir == 1
        nameFolder = filelist(indexFile).name;
        if nameFolder(1) ~= '.'
            folderList(indexFolder) = filelist(indexFile);
            folderList(indexFolder).name = strcat(folderList(indexFolder).name, '/');
            indexFolder = indexFolder + 1;
        end
    end
end

% Read data in each folder
for indexFolder = 1:length(folderList)
    if length(folderList) ~= 1
        folder2 = strcat(folder1, folderList(indexFolder).name);
    else
        folder2=folder1;
    end
    filelist = dir(folder2);  
    
    indexFile = 1;
    while indexFile <= length(filelist)
        if filelist(indexFile).isdir == 1
            filelist = filelist([1:indexFile-1 indexFile+1:end]);   % skip folders
        else
            indexFile = indexFile + 1;
        end
    end

    for indexFile = 1:(length(filelist))
        nameFolder = filelist(indexFile).name;
        if nameFolder(1) ~= '.'
            info = dicominfo([folder1 filelist(indexFile).name]);
            frames = info.PerFrameFunctionalGroupsSequence;
            info2 = frames.Item_1.Private_2005_140f.Item_1;
            
            %Image position
            info_loc = info2.ImagePositionPatient; %position of the top left corner voxel of the first image (used for the 3 loops after)
            
            %Patient's orientation
            patient_Orient = reshape(info.PerFrameFunctionalGroupsSequence.Item_1.PlaneOrientationSequence.Item_1.ImageOrientationPatient, [3 2]);
            
            S = info_loc;
            
            clear X Y Z
            for indexY = 1:matrixSize(2)
                for indexX = 1:matrixSize(1)
                    for indexZ = 1:matrixSize(3)
                        
                        %The 4x4 matrix calculating the transformation
                        M = [patient_Orient(1,1)*voxelSize(1) patient_Orient(1,2)*voxelSize(3) 0 S(1) ; patient_Orient(2,1)*voxelSize(1) patient_Orient(2,2)*voxelSize(3) 0 S(2) ; patient_Orient(3,1)*voxelSize(1) patient_Orient(3,2)*voxelSize(3) 0 S(3) ; 0 0 0 1];
                        vect = [indexX ; indexZ ; 0 ; 1];
                        P = M*vect; %The calculated position of the voxel
                        
                        X(indexX, indexY, indexZ) = P(1);
                        Y(indexX, indexY, indexZ) = P(2);
                        Z(indexX, indexY, indexZ) = P(3);
                    end
                end
                S(1) = S(1) + voxelSize(2); %updating the S(1) for each slice
            end
        end
    end
end





