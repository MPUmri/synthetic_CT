%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the spatial position from a DICOM file                        %
% for a CT scan                                                           %
%                                                                         %
% Marc-Antoine Fortin, McGill Univeristy                                  %
% 2018                                                                    %
% ----------------------------------------------------------------------- %
% Inputs:                                                                 %
% Data folder & CT scan in mat files                                      %
% ----------------------------------------------------------------------- %
% Output:                                                                 %
% Output contains the position (in mm) X, Y & Z in                        %
% the spatial coordinate system of the CT image                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [X_CT, Y_CT, Z_CT, CT_voxelSize, CT_matrixSize] = getXYZ_CT(folder_patient, ct_scan)
    %%

    CT_data = dicominfo(folder_patient);

    CT_img_posi = double(CT_data.ImagePositionPatient); %position of the top left corner voxel of the first image (used for the 3 loops after)
    S = CT_img_posi; %to be consistent with the getXYZ function
    CT_img_orient = double(reshape(CT_data.ImageOrientationPatient, [3 2])); %orientation of the patient

    CT_voxelSize(1) = double(CT_data.PixelSpacing(1)); %x
    CT_voxelSize(2) = double(CT_data.PixelSpacing(2)); %y
    CT_voxelSize(3) = double(CT_data.SliceThickness); %z

    CT_matrixSize(1) = double(CT_data.Width);
    CT_matrixSize(2) = double(CT_data.Height);
    CT_matrixSize(3) = double(size(ct_scan,3)); 


    for indexZ = 1:CT_matrixSize(3)
        for indexX = 1:CT_matrixSize(1)
            for indexY = 1:CT_matrixSize(2)
                %The 4x4 matrix calculating the transformation
                M = [CT_img_orient(1,1)*CT_voxelSize(1) CT_img_orient(1,2)*CT_voxelSize(2) 0 S(1) ; CT_img_orient(2,1)*CT_voxelSize(1) CT_img_orient(2,2)*CT_voxelSize(2) 0 S(2) ; CT_img_orient(3,1)*CT_voxelSize(1) CT_img_orient(3,2)*CT_voxelSize(2) 0 S(3) ; 0 0 0 1];
                vect = [double(indexX) ; double(indexY) ; 0 ; 1];
                P = M*vect; %The calculated position of the voxel

                X_CT(indexX, indexY, indexZ) = P(1);
                Y_CT(indexX, indexY, indexZ) = P(2);
                Z_CT(indexX, indexY, indexZ) = P(3);
            end
        end
        S(3) = S(3) + CT_voxelSize(3); %updating the S(3) for each slice
    end
    
end


