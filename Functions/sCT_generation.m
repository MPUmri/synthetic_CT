%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Synthetic CT generation                                                 %
% Veronique Fortier                                                       %
% 2018                                                                    %
% ------------------------------------------------------
% Inputs:
%   -QSM map
%   -Matrix size
%   -Mask of air and bone regions
%   -Susceptibility threshold for air identification
%   -Mask of the whole object
%   -Fat fraction map
%   -Normalized magnitude signal (fat)
%   -Normalized magnitude signal (water)
%   -Air regions parameter (0 or 1, set to 1 if air regions are expected in
%   the object)
%   -Water fraction map
%
%
% Outputs:
%   -Synthetic CT
%   -mean susceptibility in air regions
%   -standard deviation of the susceptibility in air regions
%   -mean susceptibility in bone regions
%   -standard deviation of the susceptibility in air regions
%   -Bone mask
%   -Air mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ sCT_segmented,meanAirSusc, stdAirSusc, meanBoneSusc stdBoneSusc, maskBone, air] = sCT_generation(iterateQSM, matrixSize, maskAirBone, airThreshold, maskObject, fatMap, fatImageNorm, waterImageNorm, airRegions, w1)

    %% Initialize matrix
    iterateQSM = real(iterateQSM);
    iterateQSM = double(iterateQSM);

    maskObject(:,:,1) = 0;
    maskObject(:,:,end) = 0;
    maskObject(1,:,:) = 0;
    maskObject(end,:,:) = 0;
    maskObject(:,1,:) = 0;
    maskObject(:,end,:) = 0;

    air = zeros(matrixSize);
    air1 = zeros(matrixSize);
    otherProb = zeros(matrixSize);
    boneProb = zeros(matrixSize);
    boneProbCombined1 = zeros(matrixSize);

    % Segmentation of air regions based on QSM only
    if airRegions == 1
        % Identify air regions, assume continuous regions (closing + erode + dilate + remove isolated pixels)
        air1((iterateQSM.*maskAirBone) >= airThreshold) = 1;

        for index = 1:matrixSize(3)
            air1(:,:,index) = imfill(air1(:,:,index), 'holes'); 
        end

        for index = 1:matrixSize(2)
            air1(:,index,:) = imfill(air1(:,index,:), 'holes');
        end

        for index = 1:matrixSize(1)
            air1(index,:,:) = imfill(air1(index,:,:), 'holes');
        end

    %     figure(); montage(reshape4montage(air1), [0 1]) %visualize what is identify as air regions

        se = strel('square', 3);   
        air = imerode(air1, se);
    %     figure(); montage(reshape4montage(air), [0 1]) %visualize what is identify as air regions
        se = strel('square', 30);
        air = imdilate(air, se).*maskObject;
        air = air.*air1;

    %     figure(); montage(reshape4montage(air), [0 1]) %visualize what is identify as air regions

        for index = 1:matrixSize(3)
            air(:,:,index) = imfill(air(:,:,index), 'holes');
            air(:,:,index) = bwareaopen(air(:,:,index), 6); 
        end

        for index = 1:matrixSize(2)
            air(:,index,:) = imfill(air(:,index,:), 'holes');
            air(:,index,:) = bwareaopen(air(:,index,:), 6);
        end

        for index=1:matrixSize(1)
            air(index,:,:) = imfill(air(index,:,:), 'holes');
            air(index,:,:) = bwareaopen(air(index,:,:), 6);
        end
    end

    % figure(); montage(reshape4montage(air), [0 1]) %visualize what is identify as air regions

    % Remove air from the object mask
    boneOther = logical(maskObject-air);


    %% FCM clustering on bone regions based on QSM (min susceptibility=bone)
    boneOther1 = logical((maskAirBone-air).*maskAirBone);
    boneOtherSusceptibility = iterateQSM(boneOther1);
    [centersClass,probabilityClass] = fcm(boneOtherSusceptibility, 2);

    [maximum, indiceOther] = max(centersClass);
    otherProb(boneOther1) = probabilityClass(indiceOther,:);
    [minimum,indiceBone] = min(centersClass);
    boneProb(boneOther1) = probabilityClass(indiceBone,:);


    %% Correct bone probabilities from QSM to get a better bone continuity and soft tissue contrast, using fat and water images
    fatImageNorm(isnan(fatImageNorm)) = 0;
    fatImageNorm = double(fatImageNorm);
    waterImageNorm(isnan(waterImageNorm)) = 0;
    waterImageNorm = double(waterImageNorm);

    % Fuzzy c-mean clustering on the sum of water and fat images (min signal =
    % bone)
    sumWaterFat = (fatImageNorm + waterImageNorm).*maskObject;
    boneOtherSusceptibility = sumWaterFat(boneOther);
    [centersClassMagn, probabilityClassMagn] = fcm(boneOtherSusceptibility, 5);
    [minimum, indiceBone] = min(centersClassMagn);
    boneProbCombined1(boneOther) = probabilityClassMagn(indiceBone,:);


    %% Bone regions to add based on fat and water images
    correctBoneQSM = zeros(matrixSize);
    correctBoneQSM(boneProbCombined1 > 0.5) = 1; 
    % figure(); montage(reshape4montage(boneProbCombined1), [0 1]);

    correctBoneQSM = correctBoneQSM-boneOther1;
    correctBoneQSM(find(correctBoneQSM == -1)) = 0;
    SE = strel('disk',2);     
    correctBoneQSM = logical(correctBoneQSM.*imerode(maskObject,SE)); %Remove regions outside of the object
    % figure(); montage(reshape4montage(correctBoneQSM), [0 1]); title('Parts added to the bone mask');

    maskBone = (correctBoneQSM+boneOther1).*(maskObject - air);

    maskHead = maskObject - maskAirBone;

    % Bone regions to remove based on water map (high water fraction = not bone)
    correctBoneQSM_remove = zeros(matrixSize);
    correctBoneQSM_remove(w1 > 75) = 1; 
    correctBoneQSM_remove = correctBoneQSM_remove.*maskHead;
    % figure(); montage(reshape4montage(w1.*maskObject), [0 100]);

    correctBoneQSM_remove = logical(correctBoneQSM_remove.*maskBone);
    % figure(); montage(reshape4montage(correctBoneQSM_remove), [0 1]); title('Parts removed from the bone mask');

    maskBone = logical(maskBone - correctBoneQSM_remove);
    % figure(); montage(reshape4montage(maskBone), [0 1]);

    maskBone = bwareaopen(maskBone, 4, 4); 

    % Define bone probability using a combination of QSM and fat/water images
    boneProb2 = zeros(matrixSize);
    boneProb2(find(boneProb>boneProbCombined1)) = boneProb(find(boneProb > boneProbCombined1));
    boneProb2(find(boneProb<=boneProbCombined1)) = boneProbCombined1(find(boneProb <= boneProbCombined1));

    boneProb2 = boneProb2.*maskBone;
    otherProb2 = (1 - boneProb2).*(maskBone);


    %% Soft tissue contrast based on the fat fraction map (outside of bone and air regions)
    fatMask = logical((maskObject - (maskBone + air)));
    fatProb = zeros(matrixSize);
    waterProb = zeros(matrixSize);
    fatProb(fatMask) = fatMap(fatMask) / 100;
    waterProb(fatMask) = 1 - fatProb(fatMask);


    %% sCT bulk HU assignment for air and variable HU for other
    sCT_segmented = otherProb2*30 + boneProb2*1500 + waterProb*30 + fatProb*-100;

    % Assign HU (constant for air)
    air(air==1) = -1000;
    sCT_segmented = sCT_segmented + air;
    sCT_segmented(logical(1 - maskObject)) =- 1000;


    %% Calculate mean and std of susceptibility for air and bone
    air2 = logical(air);
    meanAirSusc = mean(iterateQSM(air2))
    stdAirSusc = std(iterateQSM(air2))

    meanBoneSusc = mean(iterateQSM(sCT_segmented > 200))
    stdBoneSusc = std(iterateQSM(sCT_segmented > 200))
    % figure(); montage(reshape4montage(permute(sCT_segmented, [1,2,3])), [-300 300])


end

